﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace x.tiss.conciliatratarcargaxml
{
    public class Processador
    {
        private CancellationTokenSource _token = new CancellationTokenSource();
        private List<Task> _tasks = new List<Task>();

        public void Start()
        {
            Program.Log.Info(Program._nm_servico + ".Processador - Start: Iniciando o serviço");

            this._tasks.Add(Task.Run(() =>
            {
                Executor recebe = new Executor();
                recebe.Token = this._token.Token;

                recebe.Start();

            }, this._token.Token));
        }


        public void Stop()
        {
            _token.Cancel();

            Task.WaitAll(this._tasks.ToArray());

            Program.Log.Info(Program._nm_servico + ".Processador - Stop: Serviço parado e tasks canceladas");
        }

    }
}
