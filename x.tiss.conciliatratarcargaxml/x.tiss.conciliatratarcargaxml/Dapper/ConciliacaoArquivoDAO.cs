﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x.tiss.conciliatratarcargaxml.Dapper
{
    public class ConciliacaoArquivoDAO
    {
        public string _ConnString { get; set; }

        public ConciliacaoArquivoDAO(string ConnString)
        {
            _ConnString = ConnString;
        }

        public void InserirConciliacaoArquivo(string nom_arquivo
                                             , string caminho_arquivo
                                             , string url_arquivo
                                             , string cod_tipo_arquivo
                                             , string cod_prestador
                                             , string cod_operadora
                                             , string cod_usuario
                                             , string id_envio_automatico
                                              )
        {
            using (IDbConnection db = new SqlConnection(_ConnString))
            {
                try
                {
                    db.Open();
                    var p = new DynamicParameters();

                    p.Add("@NOM_ARQUIVO", nom_arquivo, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add("@CAMINHO_ARQUIVO", caminho_arquivo, dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add("@URL_ARQUIVO", url_arquivo, dbType: DbType.String, direction: ParameterDirection.Input);

                    p.Add("@COD_TIPO_ARQUIVO", cod_tipo_arquivo, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add("@COD_PRESTADOR", cod_prestador, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add("@COD_OPERADORA", cod_operadora, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add("@COD_USUARIO", cod_usuario, dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add("@ID_ENVIO_AUTOMATICO", id_envio_automatico, dbType: DbType.Int32, direction: ParameterDirection.Input);

                    string query = @"INSERT INTO  dbo.CONCILIACAO_ARQUIVO
                                    ( 
                                      NOM_ARQUIVO ,
                                      CAMINHO_ARQUIVO,
                                      URL_ARQUIVO,
                                      COD_TIPO_ARQUIVO ,
                                      DAT_CRIACAO ,
                                      COD_PRESTADOR ,
                                      COD_OPERADORA ,
                                      COD_USUARIO,
                                      ID_ENVIO_AUTOMATICO
                                    )
                            VALUES  ( 
                                      @NOM_ARQUIVO,
                                      @CAMINHO_ARQUIVO,
                                      @URL_ARQUIVO,
                                      2 , 
                                      GETDATE() , 
                                      @COD_PRESTADOR , 
                                      @COD_OPERADORA ,
                                      @COD_USUARIO,
                                      @ID_ENVIO_AUTOMATICO
                                    )";

                    db.Execute(query, p, commandType: CommandType.Text);
                }
                finally
                {
                    if (db.State == ConnectionState.Open) db.Close();
                }
            }
        }



    }
}
