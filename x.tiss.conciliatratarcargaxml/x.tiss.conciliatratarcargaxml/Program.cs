﻿using NLog;
using System;
using System.Configuration;
using System.Net;
using Topshelf;

namespace x.tiss.conciliatratarcargaxml
{
    class Program
    {
        public static Logger Log = LogManager.GetCurrentClassLogger();
        public static string ConnString = string.Empty;
        public static string _nm_servico = ConfigurationManager.AppSettings["service_name"].ToString();


        static void Main(string[] args)
        {
            Program.ConnString = ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //
            try
            {
                HostFactory.Run(x =>
                {
                    x.Service<Processador>(s =>
                    {
                        s.ConstructUsing(name => new Processador());
                        s.WhenStarted(tc => tc.Start());
                        s.WhenStopped(tc => tc.Stop());
                    });

                    x.RunAsLocalSystem();

                    x.SetServiceName(ConfigurationManager.AppSettings["service_name"].ToString());
                    x.SetDisplayName(ConfigurationManager.AppSettings["service_display_name"].ToString());
                    x.SetDescription("Tratar os arquivos do concilia");
                });
            }
            catch (Exception ex)
            {
                Program.Log.Fatal("Erro ao iniciar o programa, ex.: " + ex.ToString());

                Console.WriteLine("Falha ao iniciar a aplicação, ex.:" + ex.ToString());
                Console.ReadLine();
            }
        }
    }
}
