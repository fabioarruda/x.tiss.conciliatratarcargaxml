﻿using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using NLog;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using x.tiss.conciliatratarcargaxml.Dapper;
using x.tiss.conciliatratarcargaxml.Model;
using X.Tiss.Content.Models;
using X.Tiss.MQ.Utilitario;

namespace x.tiss.conciliatratarcargaxml
{
    public class Executor
    {
        public CancellationToken Token { get; set; }

        public static Logger Log = LogManager.GetCurrentClassLogger();
        private string _fila_entrada_key = ConfigurationManager.AppSettings["fila_entrada_key"].ToString();
        private IConnection _conn = null;
        private IModel _canal = null;
        private string _mq_host = ConfigurationManager.AppSettings["mq_host"].ToString();
        private int _mq_porta = Convert.ToInt32(ConfigurationManager.AppSettings["mq_porta"].ToString());
        private string _mq_usuario = ConfigurationManager.AppSettings["mq_usuario"].ToString();
        private string _mq_senha = ConfigurationManager.AppSettings["mq_senha"].ToString();

        public void Start()
        {
            Program.Log.Info(Program._nm_servico + ".Executor - Start: Iniciando os objetos de conexão");

            //Teste();

            try
            {
                using (this._conn = new MQConnect(_mq_host, _mq_porta, _mq_usuario, _mq_senha).Get().CreateConnection())
                using (this._canal = this._conn.CreateModel())
                {
                    this.ProcessarMensagens();

                    while (!this.Token.IsCancellationRequested && this._conn.IsOpen)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                    }
                }

                if (!this.Token.IsCancellationRequested)
                {
                    this._conn = null;
                    this._canal = null;

                    this.Start();
                }
            }
            catch (Exception ex)
            {
                Program.Log.Fatal(Program._nm_servico + ".Executor - Start: Falha no processamento das mensagens, veja o log para mais informações. Erro: {0}", ex.ToString());
                this.Start();
            }
        }

        private void Teste()
        {
            string msg = "{\"prestador\":\"CC53AC64-D155-4DFD-AEF3-ACA6D59F7617\",\"id\":\"5F6AD5C3-842A-42E9-8C5C-96C11D01046D\",\"integracao\":true}";
            this.Processar(msg);
        }

        private void ProcessarMensagens()
        {
            try
            {
                Program.Log.Info(Program._nm_servico + ".Executor - ProcessarMensagens: Iniciando o processo de escuta da fila: " + this._fila_entrada_key);

                this._canal.QueueDeclare(queue: this._fila_entrada_key,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                this._canal.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
                var consumer = new EventingBasicConsumer(this._canal);
                int maxConcurrency = Convert.ToInt32(ConfigurationManager.AppSettings["Nro_Tarefas_Simultaneas"]);
                SemaphoreSlim concurrencySemaphore = new SemaphoreSlim(maxConcurrency);

                consumer.Received += (model, ea) =>
                {
                    concurrencySemaphore.Wait();
                    Task.Factory.StartNew(() =>
                    {
                        var message = Encoding.UTF8.GetString(ea.Body);

                        Program.Log.Info(Program._nm_servico + ".Executor - ProcessarMensagens: Mensagem recebida: " + message);
                        this.Processar(message);
                        this._canal.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    }
                    , TaskCreationOptions.LongRunning)
                    .ContinueWith((task) => concurrencySemaphore.Release());
                };

                this._canal.BasicConsume(queue: this._fila_entrada_key,
                                         autoAck: false,
                                         consumer: consumer);

            }
            catch (Exception ex)
            {
                Program.Log.Fatal(Program._nm_servico + ".Executor - ProcessarMensagens: Falha no processamento das mensagens o serviço será finalizado, erro:" + ex.ToString());
            }
        }

        public void Processar(string msg)
        {
            Mensagem mensagem = new Mensagem();

            try
            {
                Program.Log.Info("X.Tiss.Servico.Importador.Executar - Enviar{msg: " + msg.Substring(0, 50) + "...}: Inicio do processamento da mensagem");
                string _dir_base = ConfigurationManager.AppSettings["dir_base"];
                //string _dir_schema = ConfigurationManager.AppSettings["dir_schema"];

                mensagem = JsonConvert.DeserializeObject<Mensagem>(msg);

                /*Realizar a extração do arquivo*/
                this.Extrair(mensagem.CaminhoArquivo, mensagem.Arquivo);

                /*Depois de Extrair o arquivo buscar todos arquivos xml*/
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(mensagem.CaminhoArquivo);
                
                Dictionary<string, string> dicDiretoriosCompactar = new Dictionary<string, string>();

                foreach (System.IO.FileInfo f in di.GetFiles())
                {
                    if (f.Extension.ToLower() == ".xml")
                    {
                        var dadosArquivo = ExtairDadosArquivo(mensagem.CaminhoArquivo, f.Name);
                        var operadora = new X.Tiss.Content.Models.Operadora().Carregar(dadosArquivo.Cod_Ans.ToString()
                                , ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString());

                        var diretorioDestino = Path.Combine(mensagem.CaminhoArquivo, operadora.COD_OPERADORA.ToString());

                        if (!Directory.Exists(diretorioDestino))
                        {
                            Directory.CreateDirectory(diretorioDestino);
                            dicDiretoriosCompactar.Add(operadora.COD_OPERADORA.ToString(), diretorioDestino);
                        }

                        var sOrigem = Path.Combine(mensagem.CaminhoArquivo, f.Name);

                        File.Move(sOrigem, Path.Combine(diretorioDestino, f.Name));
                    }
                }

                foreach (var compactar in dicDiretoriosCompactar)
                {
                    FastZip zip = new FastZip();

                    var sNomeArquivo = "arquivo-faturamento_" + DateTime.Now.ToString("yyyyMMddHHMMss") + "_" + compactar.Key + ".zip";

                    var sCaminhoNovoNomeArquivo 
                        = Path.Combine(mensagem.CaminhoArquivo, sNomeArquivo);

                    zip.CreateZip(sCaminhoNovoNomeArquivo, compactar.Value, true, null);

                    /*Depois de compactado deletar o diretório*/
                    Directory.Delete(Path.Combine(mensagem.CaminhoArquivo, compactar.Key), true);

                    var prestador = new X.Tiss.Content.Models.Prestadores().Carregar
                        (Guid.Parse(mensagem.prestador)
                            , ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString());
                    
                    ConciliacaoArquivoDAO daoConciliacaoArquivo 
                                    = new ConciliacaoArquivoDAO(ConfigurationManager.ConnectionStrings["EasyTiss_App_Glosa"].ToString());

                    var idTipoArquivo = (int)x.tiss.conciliatratarcargaxml.Model.TipoArquivo.Arquivo_de_faturamento;

                    var _codusuario
                        = new X.Tiss.Content.Models.Prestadores_Usuarios().Carregar
                                        (Guid.Parse(mensagem.usuario)
                                        , ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString()).COD_USUARIO_PRESTADOR;

                    daoConciliacaoArquivo.InserirConciliacaoArquivo
                        (
                         sNomeArquivo
                        , sCaminhoNovoNomeArquivo
                        , mensagem.UrlArquivo + "/" + sNomeArquivo
                        , idTipoArquivo.ToString()
                        , prestador.COD_PRESTADOR.ToString()
                        , compactar.Key
                        , _codusuario.ToString()
                        , mensagem.id
                        );

                }

                new X.Tiss.Content.Models.Recebimento_Concilia().Atualizar(Convert.ToInt32(mensagem.id)
                                , ConfigurationManager.ConnectionStrings["EasyTiss_App"].ToString());


            }
            catch (Exception ex)
            {
                Program.Log.Error("X.Tiss.Servico.Importador.Executar - Erro ao inserir o Lote: " + ex.Message);
            }
        }

        private void Extrair(string caminho, string nomeArquivo)
        {
            var pathCompleto = Path.Combine(caminho, nomeArquivo);

            FastZip zip = new FastZip();
            zip.ExtractZip(pathCompleto, caminho, null);
        }

        private X.Tiss.Content.Models.DadosArquivo ExtairDadosArquivo( string CaminhoDoArquivo, string NomeArquivo  )
        {
            DadosArquivo arq_info = new DadosArquivo();

            try
            {
                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo " + Path.Combine(CaminhoDoArquivo, NomeArquivo) + " : Extraindo informações do arquivo");

//                string dir = Path.Combine(CaminhoDoArquivo, NomeArquivo);

                string arquivo = Path.Combine(CaminhoDoArquivo, NomeArquivo);     ///Directory.GetFiles(dir).FirstOrDefault();

                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreComments = true;
                XmlReader _reader = XmlReader.Create(arquivo, settings);

                XmlDocument xml = new XmlDocument();
                xml.Load(_reader);
                string xnNode = xml.DocumentElement.ChildNodes[0].OuterXml;
                string prefix = xml.DocumentElement.Prefix + ":";

                using (XmlReader reader = XmlReader.Create(new StringReader(xnNode)
                                                          , new XmlReaderSettings()
                                                          {
                                                              IgnoreWhitespace = true
                                                                                    ,
                                                              IgnoreComments = true
                                                                                    ,
                                                              XmlResolver = null
                                                                                    ,
                                                              IgnoreProcessingInstructions = true
                                                          }
                                                          )
                                                          )
                {
                    while (reader.Read())
                    {
                        if (reader.IsStartElement())
                        {
                            string tagValor = reader.Name.Replace(prefix, string.Empty).ToLower();
                            if (tagValor.ToLower().Contains("sequencial") && tagValor.ToLower().Contains("transacao"))
                            {
                                reader.Read();
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Sequencial Transação: {0} ", reader.Value.Trim());
                                arq_info.SequencialTransacao = reader.Value.Trim();
                            }

                            if (tagValor.ToLower().Contains("padrao") && tagValor.Contains("versao"))
                            {
                                reader.Read();
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Versao: {0} ", reader.Value.Trim());
                                arq_info.Versao = reader.Value.Trim();
                            }
                            else if (tagValor.ToLower().Contains("padrao"))
                            {
                                reader.Read();
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Versao: {0} ", reader.Value.Trim());
                                arq_info.Versao = reader.Value.Trim();
                            }

                            if (tagValor.ToLower().Contains("registroans"))
                            {
                                reader.Read();
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Registro ANS: {0} ", reader.Value.Trim());
                                arq_info.Cod_Ans = Convert.ToInt32("0" + reader.Value.Trim());
                            }

                            if (tagValor.ToLower().Contains("cnpj"))
                            {
                                reader.Read();
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Cnpj: {0} ", reader.Value.Trim());
                                arq_info.TipoCodigo = DadosArquivo.TipoIdentificacao.CNPJ;
                                arq_info.CodigoPrestador = reader.Value.Trim();
                            }

                            if (tagValor.ToLower().Contains("prestadornaoperadora"))
                            {
                                reader.Read();

                                //Para versões abaixo da 3.0 o elento pode vir com ele mesmo dentro, 
                                //infelizmente isso gera um bug na leitura que acaba retornando um value null, 
                                //com o segundo read você consegue resolver o problema e carregar o valor.
                                if (string.IsNullOrEmpty(reader.Value))
                                {
                                    reader.Read();
                                }
                                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Código prestado na operadora: {0} ", reader.Value.Trim());
                                arq_info.TipoCodigo = DadosArquivo.TipoIdentificacao.Codigo;
                                arq_info.Cod_PrestadorNaOperadora = reader.Value.Trim();
                            }

                        }
                    }
                }

                arq_info.Nro_Guia = xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].ChildNodes.Count;
                if (xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].FirstChild.Name == "#comment")
                {
                    arq_info.Tipo_Guia = xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].ChildNodes[1].LocalName;
                }
                else
                {
                    arq_info.Tipo_Guia = xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].FirstChild.LocalName;
                }

                if (arq_info.Tipo_Guia.ToLower().Contains("guiafaturamento"))
                {
                    arq_info.Tipo_Guia = xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].FirstChild.FirstChild.LocalName;
                    arq_info.Nro_Guia = xml.DocumentElement.ChildNodes[1].FirstChild.ChildNodes[1].FirstChild.ChildNodes.Count;
                }
                if (arq_info.Tipo_Guia.ToLower().Contains("consulta"))
                {
                    arq_info.Tipo_Guia = "Consulta";
                }

                if (arq_info.Tipo_Guia.ToLower().Contains("sadt"))
                {
                    arq_info.Tipo_Guia = "SP/SADT";
                }

                if (arq_info.Tipo_Guia.ToLower().Contains("honorario"))
                {
                    arq_info.Tipo_Guia = "Honorario Individual";
                }

                if (arq_info.Tipo_Guia.ToLower().Contains("internacao"))
                {
                    arq_info.Tipo_Guia = "Resumo Internação";
                }

                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Tipo_Guia: {0} ", arq_info.Tipo_Guia);
                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Nro_Guia: {0} ", arq_info.Nro_Guia);

                string hash = xml.DocumentElement.ChildNodes[2].ChildNodes[0].InnerText;
                arq_info.HASH_XML = Guid.Parse(hash);
                Log.Info("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: HASH_XML: {0} ", hash);
                _reader.Close();
            }
            catch (Exception ex)
            {
                Log.Fatal("X.Tiss.Servico.Delator.RecebeArquivos - ExtairDadosArquivo: Erro: " + ex.ToString());
            }

            return arq_info;
        }


    }
}
