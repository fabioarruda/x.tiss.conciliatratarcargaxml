﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace x.tiss.conciliatratarcargaxml.Model
{
    public class Mensagem
    {
        public string prestador { get; set; }
        public string id { get; set; }
        public string CaminhoArquivo { get; set; }
        public string UrlArquivo { get; set; }
        public string Arquivo { get; set; }
        public string usuario { get; set; }
    }
}
